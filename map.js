function showMap(){
    mapInit();
     $("#map").css("display","block");
}

function mapInit() {
    var mapOptions = {
        center: new google.maps.LatLng(57.494601,12.072065),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,  
        panControl: true,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        overviewMapControl: false,
        scrollwheel:false,
        draggable:false

    };
    
    
    var map = new google.maps.Map(document.getElementById("map-container"),
    mapOptions);
}