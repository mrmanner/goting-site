/**
=======================
Configuration constants
=======================
**/
var slideTime = 500; //Time for sliding animations, in ms.

/**
=================
Google Drive URLs
=================
**/
var handlingar = 'https://docs.google.com/document/pub?id=1DvpgnLJGvpc919slYBjQtbL3oGPPV1g33e9EGfrfr80&amp;embedded=true';
var stadgar = 'https://docs.google.com/a/km.scout.se/file/d/0B1UMQ5FwOC7YS0VrM05oemk4bGs/preview';
var anmalForm = 'https://docs.google.com/spreadsheet/embeddedform?formkey=dHptSDM3QnBjQzZhUDdBTm5rYlpuMGc6MA';


/**
=====
SETUP
=====
**/

$(".infoicon").data('isSet', 0);





/**
=======================================================
Control and content loading methods for the Info block.
**/
var isOpen = new Boolean(0);    

function showInfo(divID){
    if (isOpen==false){
        $("#information").animate({height:'450px'}, slideTime, function() {
            loadInfo();
        });
        isOpen = new Boolean(1);
    }
    else if (isOpen==true){
        $("#info-container").fadeOut(function() {
            loadInfo();
        });
    }
    function loadInfo() {
        $("#info-container").load('copy/'+divID+'.htm');
        $("#info-container").fadeIn();
    }
}

function hideInfo(){
    if(isOpen){
        $("#information").animate({height:'250px'}, slideTime);
    }
    $("#info-container").fadeOut();
    isOpen = new Boolean(0);
}


/**
=================================================
Control and content loading methods for the blog.
**/

function toggleBlog() {
    $("#blog").slideToggle(slideTime);
}
  

/**
=====================================================================
Method for loading a Google document and displaying it in an overlay.

##Params
String URL: The embed URL of the document.
**/

function showDocument(url) {
    $("#overlay").html('<iframe id="overlay-doc" src="' + url + '"></iframe>');
    $("#overlay-doc").ready(function() {
        $("#overlay").fadeIn(1000);
    });
}


/**
======================================
Method emptying and hiding the overlay
**/

function closeOverlay() {
    $("#overlay").fadeOut(1000, function(){
        $("#overlay").html('empty');
    });
}
    

/**
==============================
Methods handling button input.
==============================
**/

//INFO block.
$(".infoicon").click(function() {
    if($(this).data('isSet') == 1){
        hideInfo();
        $(this).data('isSet', 0);
    } else {
        showInfo($(this).attr('id'));
        $(".infoicon").data('isSet', 0);
        $(this).data('isSet', 1);
    }
});

$("#blog-button").click(function() {
    toggleBlog();
});

$("#handlingar-button").click(function() {
    showDocument(handlingar);
});

$("#overlay").click(function() {
    closeOverlay();
});

$(".anmal-button").click(function() {
    showDocument(anmalForm);
});
    
$("#stadgar-button").click(function() {
    showDocument(stadgar);
});

$("document-button").click(function() {
    window.open("http://scout.kfum.se/blandade-dokument/");
});

    